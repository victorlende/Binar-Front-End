<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Case1</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">


  </head>
  <body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
    <a class="navbar-brand" href="#">Start Bootstrap</a>
    <ul class="navbar-nav ml-5">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">About </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Service</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Contact</a>
      </li>

    </ul>

  </div>

</nav>
<br>

<div class="container col-md-9">
  <div class="jumbotron">
  <h1 class="display-4">A Warm Welcome</h1>
  <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>

  <a class="btn btn-primary btn-lg" href="#" role="button">Call to action</a>
  </div>





    <div class="row ">
      <div class="col-md-3 text-center">
        <div class="card">
          <img src="img/img.jpg" alt="">
          <div class="card-body">
            <h4>Title card</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</p>
          <h6 class="card-title"></h6>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-primary">Find out more</button>
            </div>
        </div>
      </div>


      <div class="col-md-3 text-center">
        <div class="card">
          <img src="img/img.jpg" alt="">
          <div class="card-body">
            <h4>Title card</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</p>
          <h6 class="card-title"></h6>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-primary">Find out more</button>
            </div>
        </div>
      </div>

      <div class="col-md-3 text-center">
        <div class="card">
          <img src="img/img.jpg" alt="">
          <div class="card-body">
            <h4>Title card</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</p>
          <h6 class="card-title"></h6>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-primary">Find out more</button>
            </div>
        </div>
      </div>


      <div class="col-md-3 text-center">
        <div class="card">
          <img src="img/img.jpg" alt="">
          <div class="card-body">
            <h4>Title card</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et</p>
          <h6 class="card-title"></h6>
            </div>
            <div class="card-footer">
                <button type="button" class="btn btn-primary">Find out more</button>
            </div>
        </div>
      </div>

    </div>
</div>


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
  </body>

</html>
